import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import CarAddContainer from './components/CarAddContainer'
import CarContainer from './components/CarContainer'
import CarEditContainer from './components/CarEditContainer'
import HomepageContainer from './components/HomepageContainer'
import ItemAddContainer from './components/ItemAddContainer'
import ItemEditContainer from './components/ItemEditContainer'

export default function App () {
  return (
    <Router>
      <div className="container pt-3">
        <Switch>
          <Route path="/" exact>
            <HomepageContainer/>
          </Route>
          <Route path="/car/add" exact>
            <CarAddContainer/>
          </Route>
          <Route path="/car/edit/:id">
            <CarEditContainer/>
          </Route>
          <Route path="/car/:id">
            <CarContainer/>
          </Route>
          <Route path="/item/add" exact>
            <ItemAddContainer/>
          </Route>
          <Route path="/item/edit/:id">
            <ItemEditContainer/>
          </Route>
        </Switch>
      </div>
    </Router>
  )
}
