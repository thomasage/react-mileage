import React from 'react'
import PropTypes from 'prop-types'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { getContractEnd } from '../services/car'

export default function ChartEvolution ({ car = { leasing: false }, items }) {
  const options = {
    chart: {
      type: 'spline'
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        dataLabels: {
          enabled: true
        }
      }
    },
    series: [
      {
        data: items.map(item => [new Date(item.date).getTime(), item.mileage]),
        name: 'Evolution',
        type: 'area'
      }
    ],
    title: {
      text: 'Evolution'
    },
    xAxis: {
      title: false,
      type: 'datetime'
    },
    yAxis: {
      title: false
    }
  }

  if (car.leasing && car.contractMileageStart) {
    const contractEnd = getContractEnd(car)
    options.series.push({
      data: [
        [new Date(car.contractStart).getTime(), car.contractMileageStart],
        [contractEnd.getTime(), car.contractMileageEnd]
      ],
      name: 'Forecast'
    })
  }

  return (
    <HighchartsReact
      highcharts={Highcharts}
      options={options}
    />
  )
}

ChartEvolution.propTypes = {
  car: PropTypes.shape({
    leasing: PropTypes.bool
  }),
  items: PropTypes.array.isRequired
}
