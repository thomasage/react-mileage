import React from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { useLiveQuery } from 'dexie-react-hooks'

import { carGetAll, itemGetById, itemUpdate } from '../services/database'

import ItemEditForm from './ItemEditForm'

export default function ItemEditContainer () {
  const { id } = useParams()
  const item = useLiveQuery(() => itemGetById(id))
  const cars = useLiveQuery(carGetAll)
  const history = useHistory()

  if (!item || !cars) {
    return null
  }

  function handleCancel () {
    history.push(`/car/${item.car}`)
  }

  function handleSubmit (data) {
    itemUpdate(id, data.car, data.date, data.mileage)
      .then(() => history.push(`/car/edit/${data.car}`))
  }

  return (
    <>
      <ItemEditForm
        cars={cars}
        defaultCar={item.car}
        defaultDate={item.date}
        defaultMileage={item.mileage}
        onCancel={handleCancel}
        onSubmit={handleSubmit}
      />
    </>
  )
}
