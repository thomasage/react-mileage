import React from 'react'
import PropTypes from 'prop-types'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import { getContractEnd, getLeasingSupposedMileageAt } from '../services/car'
import { strToDate } from '../services/date'

export default function ChartGap ({ car, items }) {
  const options = {
    chart: {
      type: 'spline'
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        dataLabels: {
          enabled: true
        }
      }
    },
    series: [
      {
        data: [],
        name: 'Gap',
        type: 'area'
      }
    ],
    title: {
      text: 'Gap'
    },
    xAxis: {
      title: false,
      type: 'datetime'
    },
    yAxis: {
      plotLines: [
        {
          color: 'red',
          value: 0,
          width: 3
        }
      ],
      title: false
    }
  }

  const contractStart = strToDate(car.contractStart)
  const contractEnd = getContractEnd(car)
  for (const item of items) {
    const supposed = getLeasingSupposedMileageAt(contractStart, contractEnd, car.contractMileageStart, car.contractMileageEnd, strToDate(item.date))
    options.series[0].data.push([strToDate(item.date).getTime(), item.mileage - Math.round(supposed)])
  }

  return (
    <HighchartsReact
      highcharts={Highcharts}
      options={options}
    />
  )
}

ChartGap.propTypes = {
  car: PropTypes.shape({
    contractDuration: PropTypes.number,
    contractMileageEnd: PropTypes.number,
    contractMileageStart: PropTypes.number,
    contractStart: PropTypes.string,
    leasing: PropTypes.bool
  }),
  items: PropTypes.array.isRequired
}
