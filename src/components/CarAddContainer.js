import React from 'react'
import { useHistory } from 'react-router-dom'

import { carAdd } from '../services/database'

import CarEditForm from './CarEditForm'

export default function CarAddContainer () {
  const history = useHistory()

  function handleCancel () {
    history.push('/')
  }

  function handleSubmit (data) {
    carAdd(data)
      .then(uuid => history.push(`/car/${uuid}`))
  }

  return (
    <CarEditForm
      onCancel={handleCancel}
      onSubmit={handleSubmit}
    />
  )
}
