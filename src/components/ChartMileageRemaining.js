import React from 'react'
import PropTypes from 'prop-types'

export default function ChartMileageRemaining ({ gap, value }) {
  let text = `${Math.abs(gap)} remaining`
  if (gap > 0) {
    text = `${gap} above`
  }
  let className = ''
  if (value >= 100) {
    className = 'bg-danger'
  } else if (value > 95) {
    className = 'bg-warning'
  }

  return (
    <>
      <div className="small">Mileage</div>
      <div className="progress mt-1">
        <div className={`progress-bar progress-bar-striped progress-bar-animated ${className}`}
             role="progressbar"
             style={{ width: `${value}%` }}
             aria-valuenow={value}
             aria-valuemin="0"
             aria-valuemax="100">
          {value}%
        </div>
      </div>
      <small className="text-muted">{text}</small>
    </>
  )
}

ChartMileageRemaining.propTypes = {
  gap: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired
}
