import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export default function CarList ({ cars }) {
  return (
    <div className="row">
      {cars.map(car => (
        <div key={car.uuid} className="col-6">
          <Link to={`/car/${car.uuid}`} className="d-block btn btn-outline-secondary">
            <i className="fas fa-car fa-2x"/><br/>
            {car.name}
          </Link>
        </div>
      ))}
    </div>
  )
}

CarList.propTypes = {
  cars: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    uuid: PropTypes.string.isRequired
  })).isRequired
}
