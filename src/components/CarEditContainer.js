import React from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { useLiveQuery } from 'dexie-react-hooks'

import { carGetById, carUpdate, itemDelete, itemGetByCar } from '../services/database'

import CarEditForm from './CarEditForm'
import CarItemList from './CarItemList'

export default function CarEditContainer () {
  const { id } = useParams()
  const history = useHistory()

  const car = useLiveQuery(() => carGetById(id))
  const items = useLiveQuery(() => itemGetByCar(id))

  if (!car || !items) {
    return null
  }

  function handleCancel () {
    history.push(`/car/${id}`)
  }

  function handleItemDelete (uuid) {
    itemDelete(uuid)
      .then(() => history.push(`/car/edit/${id}`))
  }

  function handleItemEdit (uuid) {
    history.push(`/item/edit/${uuid}`)
  }

  function handleSubmit (data) {
    carUpdate(id, data)
      .then(() => history.push(`/car/${id}`))
  }

  return (
    <>
      <CarEditForm
        defaultContractDuration={car.contractDuration ?? ''}
        defaultContractMileageEnd={car.contractMileageEnd ?? ''}
        defaultContractMileageStart={car.contractMileageStart ?? ''}
        defaultContractStart={car.contractStart ?? ''}
        defaultLeasing={car.leasing ?? false}
        defaultName={car.name}
        onCancel={handleCancel}
        onSubmit={handleSubmit}
      />
      <br/><br/>
      {items.length > 0 && (
        <CarItemList
          items={items.sort((a, b) => a.date < b.date)}
          onItemDelete={handleItemDelete}
          onItemEdit={handleItemEdit}
        />
      )}
    </>
  )
}
