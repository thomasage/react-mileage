import React from 'react'
import PropTypes from 'prop-types'

export default function CarHeader ({ name }) {
  return (
    <div className="fixed-top p-2 text-center text-white bg-dark">
      <h2 className="m-0 p-0">{name}</h2>
    </div>
  )
}

CarHeader.propTypes = {
  name: PropTypes.string.isRequired
}
