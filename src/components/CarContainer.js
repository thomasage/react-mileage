import React from 'react'
import { useParams } from 'react-router-dom'
import { useLiveQuery } from 'dexie-react-hooks'

import { getLeasingStatistics } from '../services/car'
import { carGetById, itemGetByCar } from '../services/database'

import CarHeader from './CarHeader'
import CarFooter from './CarFooter'
import ChartDurationRemaining from './ChartDurationRemaining'
import ChartEvolution from './ChartEvolution'
import ChartGap from './ChartGap'
import ChartMileageRemaining from './ChartMileageRemaining'

export default function CarContainer () {
  const { id } = useParams()
  const car = useLiveQuery(() => carGetById(id))
  const items = useLiveQuery(() => itemGetByCar(id))

  if (!car || !items) {
    return null
  }

  const carStatistics = getLeasingStatistics(car, items[items.length - 1])

  return (
    <>
      <CarHeader
        name={car.name}
      />
      <div className="row mb-5 mt-4">
        {carStatistics && (
          <>
            <div className="col-6 mt-4 text-center">
              <ChartDurationRemaining
                daysRemaining={carStatistics.daysRemaining}
                value={Math.round(carStatistics.durationProgress * 1000) / 10}
              />
            </div>
            <div className="col-6 mt-4 text-center">
              <ChartMileageRemaining
                gap={Math.round(carStatistics.gap)}
                value={Math.round(carStatistics.mileageProgress * 1000) / 10}
              />
            </div>
          </>
        )}
        <div className="col-12 mt-4">
          {items.length > 0 && (
            <ChartEvolution
              car={car}
              items={items}
            />
          )}
        </div>
        <div className="col-12 mt-4">
          {car.leasing && items.length > 0 && (
            <ChartGap
              car={car}
              items={items}
            />
          )}
        </div>
      </div>
      <CarFooter
        car={car.uuid}
      />
    </>
  )
}
