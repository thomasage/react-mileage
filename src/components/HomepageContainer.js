import React from 'react'
import { Link } from 'react-router-dom'
import { useLiveQuery } from 'dexie-react-hooks'

import { carGetAll } from '../services/database'

import CarList from './CarList'

export default function HomepageContainer () {
  const cars = useLiveQuery(carGetAll)

  if (!cars) {
    return null
  }

  return (
    <>
      <CarList cars={cars}/>
      <div className="mt-5 text-center">
        <Link to="/car/add" className="btn btn-outline-primary">
          <i className="fas fa-plus"/> Add a car
        </Link>
      </div>
    </>
  )
}
