import React from 'react'
import PropTypes from 'prop-types'

export default function CarItemList ({ items, onItemDelete, onItemEdit }) {
  return (
    <table className="table table-sm small">
      <thead>
      <tr>
        <th>Date</th>
        <th>Mileage</th>
        <th style={{ width: '1px' }}/>
      </tr>
      </thead>
      <tbody>
      {items.map(item => (
        <tr key={item.uuid} className="align-middle">
          <td>{item.date}</td>
          <td>{item.mileage}</td>
          <td className="text-nowrap">
            <button type="button"
                    className="btn btn-outline-info btn-sm"
                    onClick={() => onItemEdit(item.uuid)}>
              <i className="fas fa-edit"/>
            </button>
            <button type="button"
                    className="btn btn-outline-danger btn-sm ms-2"
                    onClick={() => onItemDelete(item.uuid)}>
              <i className="fas fa-trash"/>
            </button>
          </td>
        </tr>
      ))}
      </tbody>
    </table>
  )
}

CarItemList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string.isRequired,
    mileage: PropTypes.number.isRequired,
    uuid: PropTypes.string.isRequired
  })).isRequired,
  onItemDelete: PropTypes.func.isRequired,
  onItemEdit: PropTypes.func.isRequired
}
