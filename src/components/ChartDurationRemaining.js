import React from 'react'
import PropTypes from 'prop-types'

export default function ChartDurationRemaining ({ daysRemaining, value }) {
  return (
    <>
      <div className="small">Duration</div>
      <div className="progress mt-1">
        <div className="progress-bar progress-bar-striped progress-bar-animated"
             role="progressbar"
             style={{ width: `${value}%` }}
             aria-valuenow={value}
             aria-valuemin="0"
             aria-valuemax="100">
          {value}%
        </div>
      </div>
      <small className="text-muted">{daysRemaining} days left</small>
    </>
  )
}

ChartDurationRemaining.propTypes = {
  daysRemaining: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired
}
