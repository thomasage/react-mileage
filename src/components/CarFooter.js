import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export default function CarFooter ({ car }) {
  return (
    <div className="fixed-bottom bg-dark">
      <div className="container">
        <div className="row">
          <div className="col-4">
            <Link to="/" className="d-block p-2 text-center text-white">
              <i className="fas fa-home fa-2x"/>
            </Link>
          </div>
          <div className="col-4">
            <Link to={`/car/edit/${car}`} className="d-block p-2 text-center text-white">
              <i className="fas fa-edit fa-2x"/>
            </Link>
          </div>
          <div className="col-4">
            <Link to={`/item/add?car=${car}`} className="d-block p-2 text-center text-white">
              <i className="fas fa-plus fa-2x"/>
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

CarFooter.propTypes = {
  car: PropTypes.string.isRequired
}
