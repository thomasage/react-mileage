import React, { useState } from 'react'
import PropTypes from 'prop-types'

export default function CarEditForm ({
  defaultContractDuration = '',
  defaultContractMileageEnd = '',
  defaultContractMileageStart = '',
  defaultContractStart = '',
  defaultLeasing = false,
  defaultName = '',
  onCancel,
  onSubmit
}) {
  const [data, setData] = useState({
    contractDuration: defaultContractDuration,
    contractMileageEnd: defaultContractMileageEnd,
    contractMileageStart: defaultContractMileageStart,
    contractStart: defaultContractStart,
    leasing: defaultLeasing,
    name: defaultName
  })

  function handleChange (event) {
    let value = event.target.value
    if (event.target.type === 'checkbox') {
      value = event.target.checked
    } else if (event.target.type === 'number' && value !== '') {
      value = parseInt(value)
    }
    setData(data => ({
      ...data,
      [event.target.id]: value
    }))
  }

  function handleSubmit (event) {
    event.preventDefault()
    onSubmit(data)
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="name" className="form-label">Name</label>
        <input type="text"
               id="name"
               autoFocus
               className="form-control"
               value={data.name}
               onChange={handleChange}/>
      </div>
      <div className="mt-4 form-check form-switch">
        <input
          type="checkbox"
          id="leasing"
          className="form-check-input"
          role="switch"
          checked={data.leasing}
          onChange={handleChange}
        />
        <label htmlFor="leasing" className="form-check-label">Leasing</label>
      </div>
      {data.leasing && (
        <>
          <div className="mt-3">
            <label htmlFor="contractStart" className="form-label">Contract start</label>
            <input type="date"
                   id="contractStart"
                   className="form-control"
                   value={data.contractStart}
                   onChange={handleChange}/>
          </div>
          <div className="mt-3">
            <label htmlFor="contractDuration" className="form-label">Duration</label>
            <div className="input-group">
              <input type="number"
                     min="1"
                     step="1"
                     id="contractDuration"
                     className="form-control"
                     value={data.contractDuration}
                     onChange={handleChange}/>
              <span className="input-group-text">months</span>
            </div>
          </div>
          <div className="mt-3">
            <label htmlFor="contractMileageStart" className="form-label">Mileage start</label>
            <input type="number"
                   min="0"
                   step="1"
                   id="contractMileageStart"
                   className="form-control"
                   value={data.contractMileageStart}
                   onChange={handleChange}/>
          </div>
          <div className="mt-3">
            <label htmlFor="contractMileageEnd" className="form-label">Mileage end</label>
            <input type="number"
                   min="1"
                   step="1"
                   id="contractMileageEnd"
                   className="form-control"
                   value={data.contractMileageEnd}
                   onChange={handleChange}/>
          </div>
        </>
      )}
      <div className="d-flex flex-row mt-5">
        <button type="submit" className="flex-grow-1 me-2 btn btn-success">
          <i className="fas fa-save"/> Save
        </button>
        <button type="button" className="flex-grow-1 ms-2 btn btn-outline-secondary" onClick={onCancel}>
          <i className="fas fa-times"/> Cancel
        </button>
      </div>
    </form>
  )
}

CarEditForm.propTypes = {
  defaultContractDuration: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  defaultContractMileageEnd: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  defaultContractMileageStart: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  defaultContractStart: PropTypes.string,
  defaultLeasing: PropTypes.bool,
  defaultName: PropTypes.string,
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
}
