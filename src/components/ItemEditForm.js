import React, { useState } from 'react'
import PropTypes from 'prop-types'

export default function ItemEditForm ({ defaultCar, defaultDate, defaultMileage, cars, onCancel, onSubmit }) {
  const [data, setData] = useState({
    car: defaultCar,
    date: defaultDate,
    mileage: defaultMileage
  })

  function handleChange (event) {
    let value = event.target.value
    if (event.target.type === 'number' && value !== '') {
      value = parseInt(value)
    }
    setData(data => ({
      ...data,
      [event.target.id]: value
    }))
  }

  function handleSubmit (event) {
    event.preventDefault()
    onSubmit(data)
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="car" className="form-label">Car</label>
        <select id="car"
                className="form-control"
                value={data.car}
                onChange={handleChange}>
          {cars.map(car => (
            <option key={car.uuid}
                    value={car.uuid}>
              {car.name}
            </option>
          ))}
        </select>
      </div>
      <div className="mt-3">
        <label htmlFor="date" className="form-label">Date</label>
        <input type="date"
               id="date"
               className="form-control"
               value={data.date}
               onChange={handleChange}/>
      </div>
      <div className="mt-3">
        <label htmlFor="mileage" className="form-label">Mileage</label>
        <input type="number"
               id="mileage"
               step="1"
               min="1"
               autoFocus
               className="form-control"
               value={data.mileage}
               onChange={handleChange}/>
      </div>
      <div className="d-flex flex-row mt-5">
        <button type="submit" className="flex-grow-1 me-2 btn btn-success">
          <i className="fas fa-save"/> Save
        </button>
        <button type="button" className="flex-grow-1 ms-2 btn btn-outline-secondary" onClick={onCancel}>
          <i className="fas fa-times"/> Cancel
        </button>
      </div>
    </form>
  )
}

ItemEditForm.defaultProps = {
  defaultCar: '',
  defaultDate: '',
  defaultMileage: ''
}
ItemEditForm.propTypes = {
  cars: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    uuid: PropTypes.string.isRequired
  })).isRequired,
  defaultCar: PropTypes.string,
  defaultDate: PropTypes.string,
  defaultMileage: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
}
