import React from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useLiveQuery } from 'dexie-react-hooks'

import { carGetAll, itemAdd } from '../services/database'

import ItemEditForm from './ItemEditForm'

export default function ItemAddContainer () {
  const cars = useLiveQuery(carGetAll)
  const history = useHistory()
  const location = useLocation()

  if (!cars) {
    return null
  }

  const defaultCar = new URLSearchParams(location.search).get('car')

  function handleCancel () {
    history.push(`/car/${defaultCar}`)
  }

  function handleSubmit (data) {
    itemAdd(data.car, data.date, data.mileage)
      .then(() => history.push(`/car/${data.car}`))
  }

  return (
    <>
      <ItemEditForm
        cars={cars}
        defaultCar={defaultCar}
        defaultDate={new Date().toISOString().substring(0, 10)}
        onCancel={handleCancel}
        onSubmit={handleSubmit}
      />
    </>
  )
}
