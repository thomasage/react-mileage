import { addMonthsToDate, getDaysBetweenDates, strToDate } from './date'

/**
 * @param {{contractDuration: number, contractStart: string}} car
 * @returns {Date}
 */
export function getContractEnd (car) {
  return addMonthsToDate(strToDate(car.contractStart), car.contractDuration)
}

/**
 * @param {Date} startDate
 * @param {Date} endDate
 * @param {number} startMileage
 * @param {number} endMileage
 * @returns {number}
 */
export function getLeasingDailyMileage (startDate, endDate, startMileage, endMileage) {
  const mileageAllowed = endMileage - startMileage
  const durationInDays = getDaysBetweenDates(startDate, endDate)
  return mileageAllowed / durationInDays
}

/**
 * @param {{contractDuration: number, contractMileageEnd: number, contractMileageStart: number, contractStart: string, leasing: boolean}} car
 * @param {{date: string, mileage: number}} lastItem
 * @returns {null|{daysRemaining: number, durationProgress: number, gap: number, mileageProgress: number}}
 */
export function getLeasingStatistics (car, lastItem) {
  if (!car.leasing || !lastItem) {
    return null
  }

  const contractStart = strToDate(car.contractStart)
  const contractEnd = getContractEnd(car)
  const contractDurationInDays = getDaysBetweenDates(contractStart, contractEnd)
  const nbDaysFromContractStart = getDaysBetweenDates(contractStart, new Date())

  const durationElapsedInDays = getDaysBetweenDates(contractStart, strToDate(lastItem.date))
  const contractMileage = car.contractMileageEnd - car.contractMileageStart
  const mileageForecast = contractMileage * durationElapsedInDays / contractDurationInDays

  return {
    daysRemaining: contractDurationInDays - nbDaysFromContractStart,
    durationProgress: nbDaysFromContractStart / contractDurationInDays,
    gap: lastItem.mileage - mileageForecast,
    mileageProgress: lastItem.mileage / mileageForecast
  }
}

/**
 * @param {Date} startDate
 * @param {Date} endDate
 * @param {number} startMileage
 * @param {number} endMileage
 * @param {Date} currentDate
 * @returns {Number}
 */
export function getLeasingSupposedMileageAt (startDate, endDate, startMileage, endMileage, currentDate) {
  const dailyMileage = getLeasingDailyMileage(startDate, endDate, startMileage, endMileage)
  const daysFromLeasingStart = getDaysBetweenDates(startDate, currentDate)
  return dailyMileage * daysFromLeasingStart + startMileage
}

// /**
//  * @param {Object} leasing
//  */
// getLeasingDurationInDays (leasing) {
//   return this.getDaysBetweenDates(this.strToDate(leasing.startDate), this.getLeasingEndDate(leasing))
// },
// /**
//  * @param {Object} leasing
//  * @returns {Date}
//  */
// getLeasingEndDate (leasing) {
//   const endDate = this.strToDate(leasing.startDate)
//   endDate.setMonth(endDate.getMonth() + leasing.durationInMonths)
//   return endDate
// },
