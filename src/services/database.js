import Dexie from 'dexie'
import { v4 as uuidv4 } from 'uuid'

const database = new Dexie('mileage')
database
  .version(2)
  .stores({
    cars: 'uuid,name',
    items: 'uuid,car,date'
  })

/**
 * @param {object} data
 * @returns {Promise}
 */
export function carAdd (data) {
  return database.cars.put({
    ...data,
    uuid: uuidv4()
  })
}

/**
 * @returns {Promise}
 */
export function carGetAll () {
  return database.cars.toArray()
}

/**
 * @param {string} id
 * @returns {Promise}
 */
export function carGetById (id) {
  return database.cars.get(id)
}

/**
 * @param {string} uuid
 * @param {object} data
 * @returns {Promise}
 */
export function carUpdate (uuid, data) {
  return database.cars.update(uuid, data)
}

/**
 * @param {string} car
 * @param {string} date
 * @param {number} mileage
 * @returns {Promise}
 */
export function itemAdd (car, date, mileage) {
  return database.items
    .where('date').equals(date)
    .toArray()
    .then(items => items.find(item => item.car === car))
    .then(item => {
      if (item) {
        return database.items.update(item.uuid, { mileage })
      }
      return database.items.put({
        car,
        date,
        mileage,
        uuid: uuidv4()
      })
    })
}

/**
 * @param {string} uuid
 * @returns {Promise}
 */
export function itemDelete (uuid) {
  return database.items.delete(uuid)
}

/**
 * @param {string} car
 * @returns {Promise}
 */
export function itemGetByCar (car) {
  return database.items
    .where('car').equals(car)
    .sortBy('date')
}

/**
 * @param {string} id
 * @returns {Promise}
 */
export function itemGetById (id) {
  return database.items.get(id)
}

/**
 * @param {string} uuid
 * @param {string} car
 * @param {string} date
 * @param {number} mileage
 * @returns {Promise}
 */
export function itemUpdate (uuid, car, date, mileage) {
  return database.items.update(uuid, { car, date, mileage })
}
