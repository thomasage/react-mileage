/**
 * @param {Date} date
 * @param {number} months
 * @returns {Date}
 */
export function addMonthsToDate (date, months) {
  const endDate = new Date(date.getTime())
  endDate.setMonth(date.getMonth() + months)
  return endDate
}

/**
 * @param {Date} start
 * @param {Date} end
 * @returns {number}
 */
export function getDaysBetweenDates (start, end) {
  const oneDay = 24 * 60 * 60 * 1000
  return Math.round(Math.abs((end - start) / oneDay))
}

/**
 * @param {String} date
 * @returns {Date}
 */
export function strToDate (date) {
  const newDate = date.substr(0, 10).split('-')
  return new Date(parseInt(newDate[0]), newDate[1] - 1, parseInt(newDate[2]), 12, 0, 0)
}

// /**
//  * @param {Date} date
//  * @returns {String}
//  */
// formatDate (date) {
//   return date.toLocaleDateString()
// }

// /**
//  * @param {Date} date
//  * @returns {String}
//  */
// humanDate (date) {
//   if (date.toISOString().substr(0, 10) === new Date().toISOString().substr(0, 10)) {
//     return 'Today'
//   }
//   return moment(date).fromNow()
// },
